# UWSGI Daemon

__NB__: суммарный размер докер образа 547 МБ

### Не баг, а фича

в соответствии с заданием обращение к демону должно осуществляться 
вот так `curl http://localhost/ip2w/176.14.221.123`,
оно так и работает, но сам скрипт ждет только запросы вида `/:ip` или `/`,
а префикс обрабатывается с помощью rewrite rule в конфиге nginx. 
это нужно учитывать при направлении запросов к локально запущенному скрипту 

### Getting started 

#### Пример использования

- если запущен демон в docker:
```commandline
:~$ curl http://localhost:8080/ip2w/176.14.221.123
{"city": "Moscow", "temp": "+20", "conditions": "небольшой дождь"}
```

- если скрипт запущен локально: 
```commandline
:~$ curl http://localhost:8080/176.14.221.123
{"city": "Moscow", "temp": "+20", "conditions": "небольшой дождь"}
```

#### Запуск в docker
- сборка образа
```commandline
docker build -t ip2w:latest .
```

- запуск контейнера
```commandline
docker run --rm -d --privileged \
--name ip2w -p 8080:80 \
-v /sys/fs/cgroup:/sys/fs/cgroup:ro \
-v `pwd`:/src \
ip2w:latest 

```
в общем то всё, демон запустился и ждет запросов 

- подключение к шеллу в контейнере:
```commandline
docker exec -it ip2w /bin/bash
```

- сборка rpm в контейнере:
```commandline
su user
cd /src
bash buildrpm.sh ip2w.spec
```
новый rpm соберется в `~/rpm/RPMS/noarch/`, его можно скопировать в /src,
тогда новый rpm заменит текущий на хосте 

- остановка контейнера:
```commandline
docker kill ip2w
```

#### Запуск на хосте

- установка зависимостей
```commandline
pip install -r requirements.txt
```
или
```commandline
pipenv install --dev
```

- запуск юнит тестов
```commandline
python -m unittest -v tests
```

- запуск скрипта локально:
```commandline
IP_INFO_TOKEN=862231d36a38c3 \
OPEN_WEATHER_MAP_TOKEN=bedd0ec7966aea967a336b67fbdca2fc \
uwsgi --http :8080 --wsgi-file ip2w.py
```
