FROM centos/systemd

RUN yum -y install epel-release \
&& yum -y install python-pip python-devel nginx gcc git rpm-build \
&& groupadd user && adduser -g user user \
&& pip install uwsgi

COPY ./nginx.conf /etc/nginx
COPY ./ip2w-0.0.1-1.noarch.rpm /tmp/

RUN rpm -ihv /tmp/ip2w-0.0.1-1.noarch.rpm \
&& systemctl enable nginx \
&& systemctl enable ip2w

USER user

RUN git config --global user.name Roman \
&& git config --global user.email email@example.com

USER root

EXPOSE 80

CMD ["/usr/sbin/init"]
