# coding=utf-8

import ConfigParser as configparser
import functools
import json
import logging
import os
import socket
import time
import urllib2

IP_INFO_TOKEN = os.getenv('IP_INFO_TOKEN')
OPEN_WEATHER_MAP_TOKEN = os.getenv('OPEN_WEATHER_MAP_TOKEN')


def retry(exception_class, retries=5, backoff=0.1):

    def deco_retry(f):

        @functools.wraps(f)
        def f_retry(*args, **kwargs):
            attempt = 0
            while attempt < retries:
                try:
                    return f(*args, **kwargs)
                except exception_class as e:
                    if e.code >= 500:  #
                        attempt += 1
                        delay = backoff * (2 ** attempt)
                        msg = "{}, Retrying in {} seconds...".format(str(e), delay)
                        logging.warning(msg)
                        time.sleep(delay)
                    else:
                        raise

            return f(*args, **kwargs)

        return f_retry  # true decorator

    return deco_retry


@retry(urllib2.URLError)
def fetch_json(url):
    result = None
    response = urllib2.urlopen(url, timeout=5)
    if response.code == 200:
        try:
            result = json.load(response)
        except ValueError:
            logging.exception('json encoding error, check IP_INFO_TOKEN')
    return result


def fetch_location(user_ip):
    """
    fetches location info by ip from http://ipinfo.io

    :param user_ip:
    :return:
    """
    logging.info('Requesting location for ip {}'.format(user_ip))
    url_template = 'https://ipinfo.io/{ip}?token={token}'
    url = url_template.format(ip=user_ip, token=IP_INFO_TOKEN)
    result = fetch_json(url)
    if result and result.get('bogon', False):
        result = None
    return result


def fetch_weather(lat, lon):
    """
    fetches weather info by geo coordinates
    :param lat:
    :param lon:
    :return:
    """
    logging.info('Requesting weather for lat={}, lon={}'.format(lat, lon))
    url_template = ('https://api.openweathermap.org/data/2.5/weather?lat={lat}&lon={lon}'
                    '&units=metric&lang=ru&appid={token}')
    url = url_template.format(lat=lat, lon=lon, token=OPEN_WEATHER_MAP_TOKEN)
    return fetch_json(url)


def rebuild_weather(weather):
    """
    target: {"city": "Moscow", "temp": "+20", "conditions": "небольшой дождь"}

    :param weather:
    :return:
    """
    logging.info('Preparing response body')
    return {
        'city': weather['name'],
        'temp': '{:+}'.format(weather['main']['temp']),
        'conditions': weather['weather'][0]['description'],
    }


def get_user_ip(env):
    """
    if path is root directory, returns user's ip
    else parses PATH_INFO and takes first (and only) part as ip

    :param env:
    :return:
    """

    path = env['PATH_INFO']

    if path == '/':
        return env['REMOTE_ADDR']

    user_ip = None
    parts = path.strip('/').split('/')
    if len(parts) == 1:
        user_ip = parts[0]
    return user_ip


def get_config(path):

    defaults = {
        'ip2w': {
            'logs': None,  # stdout by default
            'level': 'INFO',
        }
    }

    config = configparser.ConfigParser(defaults=defaults)
    config.read(path)
    return config


def set_logger(logs_path, filename='ip2w.log', level='INFO'):
    log_format = '[%(asctime)s] %(levelname).1s %(message)s'

    logfile_absolute_path = logs_path and os.path.join(logs_path, filename)

    logging.basicConfig(
        format=log_format,
        filename=logfile_absolute_path,
        level=getattr(logging, level, logging.DEBUG),
        datefmt='%Y-%m-%d %H:%M:%S'
    )


def init_settings():
    try:
        config_path = os.getenv('IP2W_CONFIG')
        config = get_config(config_path)
    except TypeError:
        set_logger(None)
        logging.warning('Could not read config file')
    else:
        logs_path = config.get('ip2w', 'logs')
        logs_level = config.get('ip2w', 'level')
        set_logger(logs_path, level=logs_level)


def handle(env):
    """
    all the work happens here

    :param env:
    :return:
    """
    try:
        user_ip = get_user_ip(env)

        if user_ip is None:
            status, headers = ('400 Bad Request', [('Content-Type', 'application/json')])
            response_body = json.dumps({'error': 'bad request'})
        else:
            socket.inet_aton(user_ip)  # raise socket.error if argument is not valid ipv4
            location = fetch_location(user_ip)
            if location is None:
                status, headers = ('422 Unprocessable Entity', [('Content-Type', 'application/json')])
                response_body = json.dumps({'error': 'unable to detect your location'})

    except urllib2.HTTPError as e:
        if 400 <= e.code < 500:
            status, headers = ('400 Bad Request', [('Content-Type', 'application/json')])
            response_body = json.dumps({'error': 'bad request'})
        else:
            status, headers = ('503 Service Unavailable', [('Content-Type', 'application/json')])
            response_body = json.dumps({'error': 'service unavailable'})
    except socket.error:
        status, headers = ('400 Bad Request', [('Content-Type', 'application/json')])
        response_body = json.dumps({'error': 'bad request, invalid ip'})
    except Exception as e:
        logging.exception(e.message)
        status, headers = ('500 Server Error', [('Content-Type', 'application/json')])
        response_body = json.dumps({'error': 'server error'})
    else:
        lat, lon = location['loc'].split(',')
        weather = fetch_weather(lat, lon)
        short_weather = rebuild_weather(weather)
        response_body = json.dumps(short_weather)
        status, headers = ('200 OK', [('Content-Type', 'application/json')])
    finally:
        return status, headers, response_body


def application(env, start_response):
    """
    just implements wsgi application interface

    :param env:
    :param start_response:
    :return:
    """

    init_settings()
    status, headers, response_body = handle(env)
    start_response(status, headers)
    return [response_body]
