# coding=utf-8

import functools
import json
import unittest

from mock import patch, MagicMock

import ip2w


def cases(cases):
    def decorator(f):
        @functools.wraps(f)
        def wrapper(*args):
            for c in cases:
                new_args = args + (c if isinstance(c, tuple) else (c,))
                f(*new_args)

        return wrapper

    return decorator


class TestApplication(unittest.TestCase):

    @patch('ip2w.urllib2.urlopen')
    @cases([{'PATH_INFO': '/', 'REMOTE_ADDR': '95.79.231.14'}, {'PATH_INFO': '/95.79.231.14'}])
    def test_success(self, mock_urlopen, env):
        response = MagicMock()
        response.read.side_effect = [
            '{"ip": "95.79.231.14", "loc": "56.3287,44.0020"}',
            '{"weather": [{"description": "пасмурно"}],"main": {"temp": 8},"name": "Nizhniy Novgorod"}'
        ]
        response.code = 200
        mock_urlopen.return_value = response
        status, headers, response_body = ip2w.handle(env)
        self.assertEqual(status, '200 OK')
        self.assertEqual(json.dumps({
            "city": "Nizhniy Novgorod",
            "conditions": "пасмурно",
            "temp": "+8"
        }), response_body)

    @patch('ip2w.urllib2.urlopen')
    def test_private_ip(self, mock_urlopen):
        response = MagicMock()
        response.read.side_effect = [
            '{"ip": "95.79.231.14", "bogon": true}'
        ]
        response.code = 200
        mock_urlopen.return_value = response

        env = {'PATH_INFO': '/10.0.1.170'}
        status, headers, response_body = ip2w.handle(env)
        self.assertEqual(status, '422 Unprocessable Entity')

    def test_wrong_argument(self):
        env = {'PATH_INFO': '/192.168.256.2'}
        status, headers, response_body = ip2w.handle(env)
        self.assertEqual(status, '400 Bad Request', 'Invalid argument test')


if __name__ == '__main__':
    unittest.main()
